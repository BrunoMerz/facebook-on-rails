class CreateFriendsLists < ActiveRecord::Migration[5.2]
  def change
    create_table :friends_lists do |t|
      t.integer :user_id
      t.integer :friend_id
      t.boolean :both_ways, default: false

      t.timestamps
    end
  end
end
