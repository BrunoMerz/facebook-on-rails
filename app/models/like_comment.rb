class LikeComment < ApplicationRecord
  belongs_to :user
  belongs_to :comment
  validate :already_exists

  def already_exists
    if LikeComment.where(id: self.user_id).take.nil? && User.where(id: self.comment_id).take.nil?
      true
    else
      return errors.add(:message, "Não se pode dar like duas vezes no mesmo comentário")
    end
end

end
