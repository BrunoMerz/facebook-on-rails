class LikePost < ApplicationRecord
  belongs_to :user
  belongs_to :post

  validate :already_exists

  def already_exists
    if LikePost.where(id: self.user_id).take.nil? && User.where(id: self.post_id).take.nil?
      true
    else
      return errors.add(:message, "Não se pode dar like duas vezes no mesmo post")
    end
end

end
