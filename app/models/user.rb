class User < ApplicationRecord

    validates :nickname, :email, presence: true
    validates :nickname, :email, uniqueness: true

    has_many :posts
    has_many :comments
    has_many :friends_lists
    
end
