class FriendsList < ApplicationRecord

    validates :user_id, :friend_id, presence: true
    validate :equal
    validate :exist

    def equal
        if self.user_id == self.friend_id
            return errors.add(:message, "Os user_id e friend_id não pode ser igual")
        else
            return true
        end
    end

    def exist
        if User.where(id: self.user_id).take.nil? || User.where(id: self.friend_id).take.nil?
            return errors.add(:message, "Um dos ou os dois dos usuários não existem")
        else
            true
        end
    end

             
    after_create do | a |
        user = a.user_id
        friend = a.friend_id
        relation = FriendsList.find_by(user_id: friend, friend_id: user)

        if relation.present?
            a.both_ways = true
            relation.both_ways = true
            a.save
            relation.save
        end  
    end

end
