class FriendsListsController < ApplicationController
  before_action :set_friends_list, only: [:show, :update, :destroy]

  # GET /friends_lists
  def index
    @friends_lists = FriendsList.all

    render json: @friends_lists
  end

  # GET /friends_lists/1
  def show
    render json: @friends_list
  end

  # POST /friends_lists
  def create
    @friends_list = FriendsList.new(friends_list_params)

    if @friends_list.save
      render json: @friends_list, status: :created, location: @friends_list
    else
      render json: @friends_list.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /friends_lists/1
  def update
    if @friends_list.update(friends_list_params)
      render json: @friends_list
    else
      render json: @friends_list.errors, status: :unprocessable_entity
    end
  end

  # DELETE /friends_lists/1
  def destroy
    @friends_list.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_friends_list
      @friends_list = FriendsList.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def friends_list_params
      params.require(:friends_list).permit(:user_id, :friend_id)
    end
end
