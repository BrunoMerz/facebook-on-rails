class LikePostsController < ApplicationController
  before_action :set_like_post, only: [:show, :update, :destroy]

  # GET /like_posts
  def index
    @like_posts = LikePost.all

    render json: @like_posts
  end

  # GET /like_posts/1
  def show
    render json: @like_post
  end

  # POST /like_posts
  def create
    @like_post = LikePost.new(like_post_params)

    if @like_post.save
      render json: @like_post, status: :created, location: @like_post
    else
      render json: @like_post.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /like_posts/1
  def update
    if @like_post.update(like_post_params)
      render json: @like_post
    else
      render json: @like_post.errors, status: :unprocessable_entity
    end
  end

  # DELETE /like_posts/1
  def destroy
    @like_post.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like_post
      @like_post = LikePost.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def like_post_params
      params.require(:like_post).permit(:post_id, :user_id)
    end
end
