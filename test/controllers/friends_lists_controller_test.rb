require 'test_helper'

class FriendsListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @friends_list = friends_lists(:one)
  end

  test "should get index" do
    get friends_lists_url, as: :json
    assert_response :success
  end

  test "should create friends_list" do
    assert_difference('FriendsList.count') do
      post friends_lists_url, params: { friends_list: { both_ways: @friends_list.both_ways, friend_id: @friends_list.friend_id, user_id: @friends_list.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show friends_list" do
    get friends_list_url(@friends_list), as: :json
    assert_response :success
  end

  test "should update friends_list" do
    patch friends_list_url(@friends_list), params: { friends_list: { both_ways: @friends_list.both_ways, friend_id: @friends_list.friend_id, user_id: @friends_list.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy friends_list" do
    assert_difference('FriendsList.count', -1) do
      delete friends_list_url(@friends_list), as: :json
    end

    assert_response 204
  end
end
