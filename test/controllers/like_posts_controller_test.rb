require 'test_helper'

class LikePostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @like_post = like_posts(:one)
  end

  test "should get index" do
    get like_posts_url, as: :json
    assert_response :success
  end

  test "should create like_post" do
    assert_difference('LikePost.count') do
      post like_posts_url, params: { like_post: { post_id: @like_post.post_id, user_id: @like_post.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show like_post" do
    get like_post_url(@like_post), as: :json
    assert_response :success
  end

  test "should update like_post" do
    patch like_post_url(@like_post), params: { like_post: { post_id: @like_post.post_id, user_id: @like_post.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy like_post" do
    assert_difference('LikePost.count', -1) do
      delete like_post_url(@like_post), as: :json
    end

    assert_response 204
  end
end
